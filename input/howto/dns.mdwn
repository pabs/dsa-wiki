# how to update DNS resource records

## updating standard resource records

For most zones, the hidden primary DNS server is denis, with RcodeZero, Netnod
and easyDNS providing public-facing secondary servers.

Zone files are managed via a [git repository][1].  Pushing commits into the git
repository will invoke a post-commit hook that causes the recompilation and
reload of the zone files.

Some subdomains (specifically www.debian.org and security.debian.org) are
served by the autodns/geodns setup on geo{1,2,3}.  Their zone files are managed
by a separate [git repository][2].

## updating DNSSEC records

When nagios complains about impending DS expiry, find the new key in
/srv/dns.debian.org/var/keys/$zone/dsset and add it at the registrar's (gandi).
Leave the old one in place for a day or so, after checking that dnsviz.net is
happy with the new key.  For the debian.org and 29.172.in-addr.arpa zones, also
update the trust anchors in puppet.

[1]: ssh://git@ubergit.debian.org/dsa/domains
[2]: ssh://git@ubergit.debian.org/dsa/auto-dns
