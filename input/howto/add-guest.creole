== Add a guest account to ud-ldap ==

Check that the new user is a Debian contributor and the request is reasonable.

Save the signed request to a file and the signed DMUP agreement to another file.

Setup the script that automates the addition of guest accounts:

{{{
	git clone https://db.debian.org/git/dsa-misc.git
	ln -s $(pwd)/scripts/add-guest ~/bin/dsa-add-guest
}}}

For a guest account based on DM or NM status:

{{{
	dsa-add-guest ~/dsa/debian-keyrings ~/dsa/guest-keyring ~/path/to/mail ~/path/to/dmup dm <fingerprint>
	dsa-add-guest ~/dsa/debian-keyrings ~/dsa/guest-keyring ~/path/to/mail ~/path/to/dmup nm <fingerprint>
}}}

For a guest account based on a sponsor:

{{{        
	dsa-add-guest ~/dsa/debian-keyrings ~/dsa/guest-keyring ~/path/to/mail ~/path/to/dmup sponsor <fingerprint>
}}}

At the git commit prompt enter something like this:

{{{
	Add Jane Doe (RT#1234)
}}}

At the final account entry prompt:

* enter fingerprint, account name, [fml] name and forwarding address.
* enter expiry date and hosts to allow access to, per the request.
  Two months is typical.

Then close the RT ticket.
