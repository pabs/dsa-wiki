using schroot in debian.org porterboxes
=======================================

When logging into Debian porterbox machine:

* List available chroots:
<pre><code>
	$ schroot -l
</code></pre>

* Begin a persistent schroot session on Debian unstable (or in the case of backports replace sid with, for example, jessie-backports):
<pre><code>
	# Pick your own session name:
	$ echo -n "Session ID: " && read sessionid && schroot -b -c sid -n $sessionid
	# or use a random one:
	$ sessionid=$(schroot -b -c sid)
</code></pre>

* Update and upgrade Debian unstable chroot:
<pre><code>
	$ dd-schroot-cmd -c $sessionid apt-get update
	$ dd-schroot-cmd -c $sessionid apt-get upgrade
	[..]
	Do it for real [Y/n]: <Enter>
	[..]
</code></pre>

* Install your build dependencies:
<pre><code>
	$ dd-schroot-cmd -c $sessionid apt-get build-dep hello
	[..]
	Do it for real [Y/n]: <Enter>
	[..]
</code></pre>

* In the case of backports (or experimental), you can install a given package. You could use, for example:
<pre><code>
	$ dd-schroot-cmd -c $sessionid apt-get install debhelper/jessie-backports
	[..]
</code></pre>

* Change root to Debian unstable chroot:
<pre><code>
	$ schroot -r -c $sessionid
</code></pre>

* Download your source:
<pre><code>
	(sid_arch-dchroot)user@porterbox:~$ apt-get source hello
</code></pre>

* You should be able to reproduce your issue, hack on the package, fix the bug. Once you are done with your work, please, finish your schroot session:
<pre><code>
	(sid_arch-dchroot)user@porterbox:~$ exit
	$ schroot -e -c $sessionid
	$ exit
</code></pre>

* List your currently open sessions:
<pre><code>
	$ schroot-list-sessions
</code></pre>

* List all currently open sessions:
<pre><code>
	$ schroot --list --all-sessions
</code></pre>

* You should be done by now. If you find any issue, please report to debian-admin@lists.debian.org.
