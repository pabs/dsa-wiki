Firewalling debian.org hosts
============================

Debian's own firewalling
------------------------

A number of hosts have incoming ssh connections restricted to some subnets.
In particular, this includes mirrors, buildds and DSA's gitolite host.  To
connect to those machines, users can hop through master.debian.org or
people.debian.org.


Third party firewalling
-----------------------

In Debian we rely on sponsors for providing housing and hosting for all
of our infrastructure.  As such, we have a lot of our gear spread out
all over the world across many different locations.

To make our life easier our general preference is that our kind sponsors
give us unfiltered internet.  That means no firewall, no blocking of any
ports or protocols, no blocking of ICMP, no protocol enforcement/cleanup
and no state tracking and killing sessions that appear to be idle.  We
are fortunate that most places are able to provide this.

We also acknowledge that sometimes local policies outside of our primary
hosting provider requires a less optimal setup (e.g. the Computer
Science department hosts our machine but central IT which controls the
University's border routers think ICMP is the devil's doing).

In these cases we usually ask for the following setup:

* allow all outgoing traffic
* allow incoming ICMP
* allow incoming tcp/22 (ssh)
* allow all incoming from
  * bytemark: 5.153.231.0/24
  * grnet: 194.177.211.192/27
  * man-da: 82.195.75.64/26
  * osuosl: 140.211.166.192/27
  * sil: 86.59.118.144/28
  * ubcece: 206.12.19.0/24
  * ubc: 209.87.16.0/24
  * bytemark: 2001:41c8:1000::/48
  * grnet: 2001:648:2ffc:deb::/64
  * man-da: 2001:41b8:202:deb::/64
  * osuosl: 2605:bc80:3010:b00::/64
  * sil: 2001:858:2:2::/64
  * ubcece: 2607:f8f0:610:4000::/64
  * ubc: 2607:F8F0:614:1::/64
* allow all return traffic on tcp/udp/etc.

Extra ports might be required for specific services.

---
Sat, 04 Nov 2017 19:34:24 +0000
