= Hardware requirements for buildds and porter boxes =

This list is not final, and if you have questions, please talk to DSA on #debian-admin on IRC or email dsa@debian.org.

== Requirements for individual hosts ==

Must have:
* Out of band management with dedicated network port, preferably a
  BMC, or failing that, serial console and networked power bars
* Rackmount
* No human intervention to power on.
* Warranty or post-warranty hardware support, preferably provided by the sponsor.
* Under the 'ownership' of Debian (a long-term loan can also work).

Would like to have:
* Production quality rather than pre-production hardware.  This can be
  waived in cases such as when there is no production hardware
  available at all.
* Support for multiple drives (so we can do RAID)

== Requirements for ports ==

Must have/be
* At least two machines to act as buildds.
* At least one machine to act as a porter box.
* Hosted in two different locations
* The machine's architecture has an actively-maintained stable kernel
  in the archive.
* Packages critical for DSA operations available: puppet, samhain,
  syslog-ng, ferm/pf, etc.


Would like to have:
* Hosted in two different legal jurisdictions.
